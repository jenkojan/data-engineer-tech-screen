# Tech Screen



## Getting started

1. **Clone** this repository to your local machine
2. **Install** `docker` and `docker compose` if you don't have them already
3. In the repository root directory, **run** the following:
```
docker compose up --force-recreate -d
```

##### Your local machine is now running 2 services:
1. **SFTP server** 
    - **port**: 2222
    - **user**: `data_engineer`
    - **auth**: public-key authentication, use `keys/id_key_test`
2. **PostgreSQL database** 
    - **port**: 5432
    - **user**: `postgres`
    - **password**: `pass`
    - **database**: `test_db`

To shut down the docker containers, run 
```
docker compose down
```

<details>
<summary> Docker Compose V1 </summary>
If you are using Docker Compose V1 (as opposed to V2), use `docker-compose` in place of `docker compose` in both commands.
</details>

## Your Task
Make a data model of a **simple trading portfolio**. 

In your programming language of choice, do the following:

1. **Create a SFTP client** that can pull files from the SFTP server
2. **Create a data pipeline**, that extracts files from the SFTP server, **processes** the data and saves it to the database (save in memory first, if you want and have extra time, go ahead and also save the records to DB)
3. **Run the pipeline** for date range from 2023-1-2 to 2023-1-13 to **populate the database** (or just save Positions and Transactions in memory)
4. **Write a function** that takes a date range as an input and outputs a JSON of the form [{"date": ..., "balance": ...}, ...] for the portfolio you just processed.


**Hints**:<br>
- Files hosted on SFTP contain information about **positions** and **transactions** on a given date. The **TRX** files include **all transactions that happend that day**, while the **POS** files show a **snapshot of the positions** at the **end of the day**.

- Think of position snapshots as **state** and transactions as **actions**, which can be applied to state in order to change it. 

- The **process** part of the data pipeline should check the **correctness** of data. There are **two ways** to get the positions' state at the end of the day and the results should match in both cases. One way is from the POS file for a given date, the other way is to take previous day's positions and apply today's transactions to them.

- Most transactions affect two positions. Buying an AAPL stock increases the AAPL quantity, but also reduces quantity of the cash Position.

## Submitting your work
Submit your solution by either
- sending the solution over email in a compressed archive (.zip or similar) **OR** 
- uploading it to your own, **private** repository.
