#!/bin/sh

echo "
-----------------------------
-- INSTALLING DEPENDENCIES --
-----------------------------";
      
docker compose exec postgres bash -c '
	apt update; 
	apt install curl unzip -y;
';

echo "
----------------------
-- DOWNLOADING DATA --
----------------------";
      
docker compose exec postgres bash -c '	
	rm /home/dvdrental.*;
	curl -o /home/dvdrental.zip https://www.postgresqltutorial.com/wp-content/uploads/2019/05/dvdrental.zip;
	cd /home; 
	unzip dvdrental.zip;
';

echo "
--------------------------------
-- CREATE dvd_rental DATABASE --
--------------------------------";
      
docker compose exec postgres bash -c 'psql -U postgres -c "CREATE DATABASE dvd_rental;" || true';
docker compose exec postgres bash -c '/usr/bin/pg_restore -U postgres -d dvd_rental /home/dvdrental.tar';
echo "Successfully loaded data";
